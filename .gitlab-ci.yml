# List of stages for jobs, and their order of execution
stages:      
  - phpunit
  - sonarqube
  - monitoring
  - scaling
  - build
  - deploy

phpunit:
  stage: phpunit
  # when: manual
  image:
    name: php:latest
  before_script:
    - apt-get update && apt-get -yq install git unzip zip libzip-dev zlib1g-dev
    - docker-php-ext-install zip
    - pecl install xdebug && docker-php-ext-enable xdebug
    - php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    - php composer-setup.php --install-dir=/usr/local/bin --filename=composer
    - composer install
    - composer require --dev phpunit/phpunit phpunit/php-code-coverage
  variables:
    XDEBUG_MODE: "coverage"
  script:
    - php ./vendor/bin/phpunit --coverage-text --coverage-cobertura=coverage.cobertura.xml
  artifacts:
    reports:
      cobertura: coverage.cobertura.xml
  only:
    - main

sonarcloud-check:
  stage: sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache 
  script:
    - sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.php.coverage.reportPaths=coverage.cobertura.xml
  allow_failure: true
 
monitoring:
  stage: monitoring
  # when: manual
  image: 
    name: alpine/k8s:1.20.7
    entrypoint: [""]
  variables:
    K8S_NAME: "appbe-eks"
    FB_Port: "2020"
    FB_ReadFromHead: "Off"
    FB_ReadFromTail: "On"
    FB_HttpServer: "On"
  before_script:
    - mkdir -p $HOME/.kube
    - cp -f ${KUBE_CONFIG} $HOME/.kube/config 
    - kubectl config use-context aws
    - aws-iam-authenticator version
    - kubectl get ns
  script: 
    - curl https://raw.githubusercontent.com/aws-samples/amazon-cloudwatch-container-insights/latest/k8s-deployment-manifest-templates/deployment-mode/daemonset/container-insights-monitoring/quickstart/cwagent-fluent-bit-quickstart.yaml | sed 's/{{cluster_name}}/'${K8S_NAME}'/;s/{{region_name}}/'${AWS_DEFAULT_REGION}'/;s/{{http_server_toggle}}/"'${FB_HttpServer}'"/;s/{{http_server_port}}/"'${FB_Port}'"/;s/{{read_from_head}}/"'${FB_ReadFromHead}'"/;s/{{read_from_tail}}/"'${FB_ReadFromTail}'"/' | kubectl apply -f - 
    - kubectl get pods -n amazon-cloudwatch
    - kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
    - kubectl get deployment metrics-server -n kube-system
  only:
    - main

scaling:
  stage: scaling
  # when: manual
  image: 
    name: alpine/k8s:1.20.7
    entrypoint: [""]
  variables:
    K8S_NAME: "appbe-eks"
    FB_Port: "2020"
    FB_ReadFromHead: "Off"
    FB_ReadFromTail: "On"
    FB_HttpServer: "On"
  before_script:
    - mkdir -p $HOME/.kube
    - cp -f ${KUBE_CONFIG} $HOME/.kube/config 
    - kubectl config use-context aws
    - aws-iam-authenticator version
    - kubectl get ns
  script: 
    - kubectl apply -f https://www.eksworkshop.com/beginner/080_scaling/deploy_ca.files/cluster-autoscaler-autodiscover.yaml
    - kubectl -n kube-system get pod
  only:
    - main

app-build:
  stage: build
  image: docker:19.03.12
  variables:
    DOCKER_REGISTRY: hub.docker.com
    BACK_APP_NAME: appbe_php
    BACK_APP_NGINX: appbe_nginx
  services:
    - docker:19.03.12-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -f Dockerfile_p -t $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$BACK_APP_NAME:$CI_COMMIT_SHORT_SHA .
    - docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$BACK_APP_NAME:$CI_COMMIT_SHORT_SHA
  
.deploy_before_script:
  before_script:
    - apk update  && apk add --no-cache curl jq python3 py-pip && pip install awscli
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    - chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl
    - curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.21.2/2021-07-05/bin/linux/amd64/aws-iam-authenticator
    - chmod +x ./aws-iam-authenticator && mv ./aws-iam-authenticator /usr/local/bin/aws-iam-authenticator
    - mkdir -p $HOME/.kube
    - cp -f ${KUBE_CONFIG} $HOME/.kube/config 
    - kubectl config use-context aws
    - aws-iam-authenticator version
    - kubectl get ns

deploy:
  stage: deploy
  image: alpine:3.15
  extends: .deploy_before_script
 
  variables:
    BACK_APP_NAME: appbe_php
    BACK_APP_NGINX: appbe_nginx
    APP_NODEPORT: 31080
    DBHOST: mysql
    APP_AS: appbe-as
    APP_NS: $CI_COMMIT_BRANCH
    HPA_MIN: 2
    HPA_MAX: 3
    HPA_CPU: 75
    IMAGEP: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$BACK_APP_NAME:$CI_COMMIT_SHORT_SHA
  
  script:
    - sed -i "s/<APP_NS>/${APP_NS}/g" k8s/k8s-db.yml
    - sed -i "s/<APP_NS>/${APP_NS}/g" k8s/k8s.yml
    - sed -i "s/<APP_NODEPORT>/${APP_NODEPORT}/g" k8s/k8s.yml
    - sed -i "s~<IMAGE_P>~${IMAGEP}~g" k8s/k8s.yml
    - sed -i "s/<HPA_MIN>/${HPA_MIN}/g" k8s/k8s.yml
    - sed -i "s/<HPA_MAX>/${HPA_MAX}/g" k8s/k8s.yml
    - sed -i "s/<HPA_CPU>/${HPA_CPU}/g" k8s/k8s.yml
    - sed -i "s/<APP_AS>/${APP_AS}/g" k8s/k8s.yml
    - sed -i "s/<DBHOST>/${DBHOST}/g" k8s/k8s.yml
    - kubectl apply -f k8s/k8s.yml -n ${APP_NS}
    - kubectl apply -f k8s/k8s-db.yml -n ${APP_NS}
    - kubectl get deploy -n ${APP_NS}
    - kubectl get hpa -n ${APP_NS}
  except:
     - main

deploy-main:
  stage: deploy
  image: alpine:3.15
  extends: .deploy_before_script

  variables:
    BACK_APP_NAME: appbe_php
    BACK_APP_NGINX: appbe_nginx
    DBHOST: mysql.cyivvsklcv5h.eu-central-1.rds.amazonaws.com
    APP_NODEPORT: 32080
    APP_AS: appbe-as
    APP_NS: main
    HPA_MIN: 3
    HPA_MAX: 5
    HPA_CPU: 75
    IMAGEP: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$BACK_APP_NAME:$CI_COMMIT_SHORT_SHA

  script:
    - sed -i "s/<APP_NS>/${APP_NS}/g" k8s/k8s.yml
    - sed -i "s/<APP_NODEPORT>/${APP_NODEPORT}/g" k8s/k8s.yml
    - sed -i "s~<IMAGE_P>~${IMAGEP}~g" k8s/k8s.yml
    - sed -i "s/<HPA_MIN>/${HPA_MIN}/g" k8s/k8s.yml
    - sed -i "s/<HPA_MAX>/${HPA_MAX}/g" k8s/k8s.yml
    - sed -i "s/<HPA_CPU>/${HPA_CPU}/g" k8s/k8s.yml
    - sed -i "s/<APP_AS>/${APP_AS}/g" k8s/k8s.yml
    - sed -i "s/<DBHOST>/${DBHOST}/g" k8s/k8s.yml
    - kubectl apply -f k8s/k8s.yml -n ${APP_NS}
    - kubectl get deploy -n ${APP_NS}
    - kubectl get hpa -n ${APP_NS}
  only:
     - main



