<?php
$db_server = getenv('MYSQL_HOST');
$db_rootname = getenv('MYSQL_ROOT_USER');
$db_rootpass = getenv('MYSQL_ROOT_PASSWORD');
$db_username = getenv('MYSQL_USER');
$db_userpass = getenv('MYSQL_PASSWORD');
$db_name = getenv('MYSQL_DATABASE');
?>
